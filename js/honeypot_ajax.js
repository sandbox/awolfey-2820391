(function ($) {
  Drupal.behaviors.honeypotRealtime = {
    attach: function (context, settings) {
      if (context == '[object HTMLDocument]') {
        var form = $('.hptime-field input').parents('form');
        form.find('.hptime-field input').each(function() {
          $(this).once('hptime-update', function () {
            $(this).trigger('hptime');
          });
        });
      }
    }
  };
})(jQuery);